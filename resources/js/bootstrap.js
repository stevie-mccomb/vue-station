import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuex from 'vuex';
import VuexPersistence from 'vuex-persist';

window.Vue = Vue;
window.VueRouter = VueRouter;
window.Vuex = Vuex;
window.VuexPersistence = VuexPersistence;

Vue.use(VueRouter);
Vue.use(Vuex);
