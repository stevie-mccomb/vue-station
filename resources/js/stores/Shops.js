export default {
    namespaced: true,

    state: {
        shops: [
            {
                id: 1,
                name: 'Ramen Noodle Stand',
                cost: 0.00,
                rate: 1.00,
                description: 'A simple ramen noodle stand. Greatness often comes from humble beginnings.',
                owned: false
            },

            {
                id: 2,
                name: 'Ice Cream Shop',
                cost: 100.00,
                rate: 5.00,
                description: 'Children and adults alike delight in this cold confectionery.',
                owned: false
            },

            {
                id: 3,
                name: 'Magazine Stand',
                cost: 500.00,
                rate: 10.00,
                description: 'Extra, extra! Read all about it!',
                owned: false
            },

            {
                id: 4,
                name: 'Petting Zoo',
                cost: 1000.00,
                rate: 50.00,
                description: 'Baa.',
                owned: false
            },

            {
                id: 5,
                name: 'Chinese Restaurant',
                cost: 5000.00,
                rate: 100.00,
                description: 'Yum.',
                owned: false
            },

            {
                id: 6,
                name: 'Jewelry Store',
                cost: 50000.00,
                rate: 500.00,
                description: 'Bling bling.',
                owned: false
            },

            {
                id: 7,
                name: 'Shopping Mall',
                cost: 100000.00,
                rate: 1000.00,
                description: 'A lot of people. Gross.',
                owned: false
            },


            {
                id: 8,
                name: 'Bank',
                cost: 500000.00,
                rate: 10000.00,
                description: 'Cha-ching',
                owned: false
            },

            {
                id: 9,
                name: 'Slots Racket',
                cost: 1000000.00,
                rate: 100000.00,
                description: 'Sneaky.',
                owned: false
            },

            {
                id: 10,
                name: 'Rocket',
                cost: 100000000.00,
                rate: 0.00,
                description: 'Blast off!',
                owned: false,
                onPurchase() {
                    alert('You win!');
                }
            }
        ]
    },

    getters: {
        shop: (state, vm, root) => (id) => {
            for (let shop of state.shops) {
                if (shop.id === id) return shop;
            }

            return null;
        }
    },

    mutations: {
        update(state, data) {
            if (!data.id) return false;

            for (let shop of state.shops) {
                if (shop.id === data.id) {
                    for (let key of Object.keys(data)) shop[key] = data[key];

                    break;
                }
            }
        }
    },

    actions: {
        purchase({ state, commit, rootState }, id) {
            for (let shop of state.shops) {
                if (shop.id === id && rootState.bank.cash >= shop.cost && !shop.owned) {
                    commit('bank/deduct', shop.cost, { root: true });
                    commit('update', { id, owned: true });

                    if (shop.onPurchase) shop.onPurchase();
                    break;
                }
            }
        }
    }
};
