export default {
    namespaced: true,

    state: {
        cash: 0
    },

    mutations: {
        deduct(state, amount) {
            state.cash -= amount;

            if (state.cash < 0) state.cash = 0;
        },

        deposit(state, amount) {
            state.cash += amount;
        }
    }
}
