const LocalStorage = new VuexPersistence({
    storage: window.localStorage
});

export default new Vuex.Store({
    modules: {
        bank: require('./Bank').default,
        shops: require('./Shops').default
    },

    plugins: [
        LocalStorage.plugin
    ]
});
