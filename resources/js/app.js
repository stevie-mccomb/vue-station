require('./bootstrap');

const components = require.context('./components', true, /\.vue$/i);
components.keys().map(key => {
    let name = key.split('/').pop().split('.')[0];

    Vue.component(name, components(key).default);
});

const routes = require('./routes').default;
const router = new VueRouter({ routes });
const store = require('./stores').default;

window.eventHub = new Vue();

const app = new Vue({
    router,
    store,

    mounted() {
        this.interval = setInterval(this.tick.bind(this), 1000);
    },

    data() {
        return {
            interval: 0,
            seconds: 0
        };
    },

    methods: {
        tick() {
            this.seconds = Math.floor(Date.now() / 1000);

            for (let shop of this.$store.state.shops.shops) {
                if (!shop.owned) continue;

                this.$store.commit('bank/deposit', shop.rate);
            }
        }
    }
}).$mount('#app');
