const mix = require('laravel-mix');

mix.options({
    processCssUrls: false,
    publicPath: 'public'
});

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');

if (mix.inProduction()) {
    mix.version();
}
